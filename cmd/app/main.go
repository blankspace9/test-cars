package main

import (
	"fmt"
	"log/slog"
	"os"
	"os/signal"
	"syscall"

	"github.com/blankspace9/task-cars/internal/app"
	"github.com/blankspace9/task-cars/internal/config"
	"github.com/blankspace9/task-cars/internal/lib/logger/sl"
)

const (
	envLocal = "local"
	envDev   = "dev"
	envProd  = "prod"
)

// @title Car API
// @version 1.0
// @description API для управления информацией о машинах
// @BasePath /api
func main() {
	cfg := config.MustLoad()

	log := setupLogger(cfg.Env)

	log.Info("starting application", slog.String("env", cfg.Env), slog.String("port", cfg.HTTPServer.Port), slog.String("timeout", cfg.HTTPServer.Timeout.String()))

	application := app.New(log, *cfg)

	application.HTTPServer.Run()

	// graceful shutdown
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGTERM, syscall.SIGINT)

	select {
	case s := <-stop:
		log.Info("stopping application", slog.String("signal", s.String()))
	case err := <-application.HTTPServer.Notify():
		sl.Err(fmt.Errorf("%w", err))
	}

	application.HTTPServer.Stop()

	log.Info("application stopped")
}

func setupLogger(env string) *slog.Logger {
	var log *slog.Logger

	switch env {
	case envLocal:
		log = slog.New(
			slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case envDev:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case envProd:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}),
		)
	}

	return log
}
