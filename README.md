# test-cars

## Getting started
* Запуск мигратора для создания таблиц БД (в .env необходимо указать конфиги для подключения к БД)  
`go run cmd/migrator/main.go --migrations-path=./migrations`
* Запуск приложения  
`go run cmd/app/main.go`