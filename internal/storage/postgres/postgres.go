package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"strings"

	"github.com/blankspace9/task-cars/internal/domain/models"
	"github.com/blankspace9/task-cars/internal/storage"
	"github.com/lib/pq"
)

type Storage struct {
	db *sql.DB
}

type PostgresConnectionInfo struct {
	Host     string
	Port     string
	Username string
	DBName   string
	SSLMode  string
	Password string
}

// New creates a new instance of the PostgreSQL storage.
func New(connectionInfo PostgresConnectionInfo) (*Storage, error) {
	const op = "storage.postgres.New"

	db, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=%s password=%s",
		connectionInfo.Host, connectionInfo.Port, connectionInfo.Username, connectionInfo.DBName, connectionInfo.SSLMode, connectionInfo.Password))
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	if err := db.Ping(); err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	return &Storage{db: db}, nil
}

func (s *Storage) AddCar(ctx context.Context, car models.Car) (int64, error) {
	const op = "storage.postgres.AddCar"

	stmt, err := s.db.Prepare("INSERT INTO cars(reg_num, mark, model, year, owner) VALUES($1, $2, $3, $4, $5) RETURNING id")
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, car.RegNum, car.Mark, car.Model, car.Year, car.Owner)

	var uid int64
	err = row.Scan(&uid)
	if err != nil {
		var pgErr *pq.Error
		if errors.As(err, &pgErr) && pgErr.Code == "23505" {
			return 0, fmt.Errorf("%s: %w", op, storage.ErrCarExists)
		}

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	return uid, nil
}

func (s *Storage) AddPeople(ctx context.Context, people models.People) (int64, error) {
	const op = "storage.postgres.AddPeople"

	stmt, err := s.db.Prepare("INSERT INTO people(name, surname, patronymic) VALUES($1, $2, $3) RETURNING id")
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, people.Name, people.Surname, people.Patronymic)

	var uid int64
	err = row.Scan(&uid)
	if err != nil {
		var pgErr *pq.Error
		if errors.As(err, &pgErr) && pgErr.Code == "23505" {
			return 0, fmt.Errorf("%s: %w", op, storage.ErrPeopleExists)
		}

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	return uid, nil
}

func (s *Storage) AddCars(ctx context.Context, cars []models.ExternalCar) error {
	const op = "storage.postgres.AddCars"

	tx, err := s.db.Begin()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	for _, car := range cars {
		ownerID, err := s.AddPeople(ctx, car.Owner)
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("%s: %w", op, err)
		}
		_, err = s.AddCar(ctx, models.Car{
			ID:     car.ID,
			RegNum: car.RegNum,
			Mark:   car.Mark,
			Model:  car.Model,
			Year:   car.Year,
			Owner:  ownerID})
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("%s: %w", op, err)
		}
	}

	if err := tx.Commit(); err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	return nil
}

func (s *Storage) GetCarByID(ctx context.Context, id int64) (models.Car, error) {
	const op = "storage.postgres.GetCarByID"

	stmt, err := s.db.Prepare("SELECT id, reg_num, mark, model, year, owner WHERE id=%1")
	if err != nil {
		return models.Car{}, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, id)

	var car models.Car
	err = row.Scan(&car.ID, &car.RegNum, &car.Mark, &car.Model, &car.Year, &car.Owner)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.Car{}, fmt.Errorf("%s: %w", op, storage.ErrCarNotFound)
		}

		return models.Car{}, fmt.Errorf("%s: %w", op, err)
	}

	return car, nil
}

func (s *Storage) GetCarsPaginate(ctx context.Context, filter models.CarUpdate, page, limit int) ([]models.Car, error) {
	const op = "storage.postgres.GetCarsPaginate"

	var cars []models.Car
	offset := (page - 1) * limit

	setValues := make([]string, 0)
	args := make([]interface{}, 0)
	argId := 1

	if filter.ID != 0 {
		setValues = append(setValues, fmt.Sprintf("id=$%d", argId))
		args = append(args, filter.ID)
		argId++
	}
	if filter.RegNum != nil {
		setValues = append(setValues, fmt.Sprintf("reg_num=$%d", argId))
		args = append(args, *filter.RegNum)
		argId++
	}
	if filter.Mark != nil {
		setValues = append(setValues, fmt.Sprintf("mark=$%d", argId))
		args = append(args, *filter.Mark)
		argId++
	}
	if filter.Model != nil {
		setValues = append(setValues, fmt.Sprintf("model=$%d", argId))
		args = append(args, *filter.Model)
		argId++
	}
	if filter.Year != nil {
		setValues = append(setValues, fmt.Sprintf("year=$%d", argId))
		args = append(args, *filter.Year)
		argId++
	}
	if filter.Owner != nil {
		setValues = append(setValues, fmt.Sprintf("owner=$%d", argId))
		args = append(args, *filter.Owner)
		argId++
	}

	var query string
	if len(args) == 0 {
		query = fmt.Sprintf("SELECT id, reg_num, mark, model, year, owner FROM cars LIMIT $%d OFFSET $%d", argId, argId+1)
	} else {
		query = fmt.Sprintf("SELECT id, reg_num, mark, model, year, owner FROM cars WHERE %s LIMIT $%d OFFSET $%d", strings.Join(setValues, " AND "), argId, argId+1)
	}
	args = append(args, limit, offset)

	stmt, err := s.db.Prepare(query)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	rows, err := stmt.QueryContext(ctx, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var car models.Car
		err := rows.Scan(&car.ID, &car.RegNum, &car.Mark, &car.Model, &car.Year, &car.Owner)
		if err != nil {
			return nil, err
		}
		cars = append(cars, car)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return cars, nil
}

func (s *Storage) UpdateCar(ctx context.Context, new models.CarUpdate) error {
	const op = "storage.postgres.UpdateCar"

	setValues := make([]string, 0)
	args := make([]interface{}, 0)
	argId := 1

	if new.RegNum != nil {
		setValues = append(setValues, fmt.Sprintf("reg_num=$%d", argId))
		args = append(args, *new.RegNum)
		argId++
	}
	if new.Mark != nil {
		setValues = append(setValues, fmt.Sprintf("mark=$%d", argId))
		args = append(args, *new.Mark)
		argId++
	}
	if new.Model != nil {
		setValues = append(setValues, fmt.Sprintf("model=$%d", argId))
		args = append(args, *new.Model)
		argId++
	}
	if new.Year != nil {
		setValues = append(setValues, fmt.Sprintf("year=$%d", argId))
		args = append(args, *new.Year)
		argId++
	}
	if new.Owner != nil {
		setValues = append(setValues, fmt.Sprintf("owner=$%d", argId))
		args = append(args, *new.Owner)
		argId++
	}

	query := fmt.Sprintf("UPDATE cars SET %s WHERE id=$%d", strings.Join(setValues, ", "), argId)
	args = append(args, new.ID)

	stmt, err := s.db.Prepare(query)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	result, err := stmt.ExecContext(ctx, args...)
	if err != nil {
		var pgErr *pq.Error
		if errors.As(err, &pgErr) && pgErr.Code == "23503" { // Foreign key violation
			return fmt.Errorf("%s: %w", op, storage.ErrPeopleNotFound)
		}
		return fmt.Errorf("%s: %w", op, err)
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("%s: %w", op, storage.ErrCarNotFound)
	}

	return nil
}

func (s *Storage) DeleteCarByID(ctx context.Context, id int64) error {
	const op = "storage.postgres.DeleteCar"

	stmt, err := s.db.Prepare("DELETE FROM cars WHERE id=$1")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	result, err := stmt.ExecContext(ctx, id)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("%s: %w", op, storage.ErrCarNotFound)
	}

	return nil
}

func (s *Storage) DeletePeople(ctx context.Context, id int64) error {
	const op = "storage.postgres.DeleteCar"

	stmt, err := s.db.Prepare("DELETE FROM people WHERE id=$1")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	result, err := stmt.ExecContext(ctx, id)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("%s: %w", op, storage.ErrPeopleNotFound)
	}

	return nil
}
