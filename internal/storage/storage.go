package storage

import "errors"

var (
	ErrPeopleExists   = errors.New("people already exists")
	ErrPeopleNotFound = errors.New("people not found")
	ErrCarNotFound    = errors.New("car not found")
	ErrCarExists      = errors.New("car already exists")
)
