package models

type Car struct {
	ID     int64  `json:"id"`
	RegNum string `json:"regNum"`
	Mark   string `json:"mark"`
	Model  string `json:"model"`
	Year   int64  `json:"year"`
	Owner  int64  `json:"owner"`
}

type CarUpdate struct {
	ID     int64   `json:"id,omitempty"`
	RegNum *string `json:"regNum,omitempty"`
	Mark   *string `json:"mark,omitempty"`
	Model  *string `json:"model,omitempty"`
	Year   *int64  `json:"year,omitempty"`
	Owner  *int64  `json:"owner,omitempty"`
}

type ExternalCar struct {
	ID     int64  `json:"id"`
	RegNum string `json:"regNum"`
	Mark   string `json:"mark"`
	Model  string `json:"model"`
	Year   int64  `json:"year"`
	Owner  People `json:"owner"`
}
