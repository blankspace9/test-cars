package rest

import (
	"context"
	"encoding/json"
	"errors"
	"log/slog"
	"net/http"
	"strconv"

	"github.com/blankspace9/task-cars/internal/domain/models"
	"github.com/blankspace9/task-cars/internal/lib/logger/sl"
	"github.com/blankspace9/task-cars/internal/services/cars"
	"github.com/gorilla/mux"
)

type Handler struct {
	log         *slog.Logger
	carsService Cars
}

// Interface for Cars service
type Cars interface {
	AddCars(ctx context.Context, regNums []string) error
	GetCars(ctx context.Context, filter models.CarUpdate, page, limit int) ([]models.Car, error)
	UpdateCar(ctx context.Context, car models.CarUpdate) error
	DeleteCarByID(ctx context.Context, id int64) error
}

func New(log *slog.Logger, carsService Cars) *Handler {
	return &Handler{
		log:         log,
		carsService: carsService,
	}
}

func (h *Handler) InitRouter() *mux.Router {
	r := mux.NewRouter()

	api := r.PathPrefix("/api").Subrouter()
	{
		api.HandleFunc("/cars", h.addCars).Methods(http.MethodPost)
		api.HandleFunc("/cars/page/{page}/limit/{limit}", h.getCars).Methods(http.MethodGet)
		api.HandleFunc("/cars/{id}", h.updateCar).Methods(http.MethodPatch)
		api.HandleFunc("/cars/{id}", h.deleteCar).Methods(http.MethodDelete)
	}

	return r
}

// @Summary Добавление машин
// @Description Добавление новых машин по указанным гос. номерам
// @Tags cars
// @Accept json
// @Produce json
// @Param regNums body []string true "Гос. номера машин"
// @Success 200 {string} string "Машины успешно добавлены"
// @Failure 400 {string} string "Ошибка при добавлении машин"
// @Router /cars [post]
func (h *Handler) addCars(w http.ResponseWriter, r *http.Request) {
	type Data struct {
		RegNums []string `json:"regNums"`
	}

	var regNums Data
	d := json.NewDecoder(r.Body)

	err := d.Decode(&regNums)
	if err != nil {
		http.Error(w, "Failed to parse JSON", http.StatusBadRequest)
		h.log.Warn("failed to parse json", sl.Err(err))
		return
	}

	err = h.carsService.AddCars(r.Context(), regNums.RegNums)
	if err != nil {
		http.Error(w, "Failed to add cars", http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// @Summary Получение машин с фильтрацией и пагинацией
// @Description Получение списка машин с применением фильтрации и пагинации
// @Tags cars
// @Accept json
// @Produce json
// @Param page path int true "Номер страницы"
// @Param limit path int true "Количество элементов на странице"
// @Param id query int false "Идентификатор машины"
// @Param reg-num query string false "Гос. номер машины"
// @Param mark query string false "Марка машины"
// @Param model query string false "Модель машины"
// @Param year query int false "Год выпуска машины"
// @Param owner query int false "ID владельца машины"
// @Success 200 {object} []models.Car "Успешный результат"
// @Failure 400 {string} string "Ошибка при получении машин"
// @Router /cars/page/{page}/limit/{limit} [get]
func (h *Handler) getCars(w http.ResponseWriter, r *http.Request) {
	pageString := mux.Vars(r)["page"]
	limitString := mux.Vars(r)["limit"]
	carIDString := r.URL.Query().Get("id")
	carRegNum := r.URL.Query().Get("reg-num")
	carMark := r.URL.Query().Get("mark")
	carModel := r.URL.Query().Get("model")
	carYearString := r.URL.Query().Get("year")
	carOwnerString := r.URL.Query().Get("owner")

	page, err := strconv.Atoi(pageString)
	if err != nil {
		http.Error(w, "Failed to get query parameter page: "+err.Error(), http.StatusBadRequest)
		h.log.Warn("failed to get page", sl.Err(err))
		return
	}

	limit, err := strconv.Atoi(limitString)
	if err != nil {
		http.Error(w, "Failed to get query parameter limit: "+err.Error(), http.StatusBadRequest)
		h.log.Warn("failed to get limit", sl.Err(err))
		return
	}

	filter := models.CarUpdate{}
	if carIDString != "" {
		carID, err := strconv.Atoi(carIDString)
		if err != nil {
			http.Error(w, "Failed to get query parameter id: "+err.Error(), http.StatusBadRequest)
			h.log.Warn("failed to get id", sl.Err(err))
			return
		}
		filter.ID = int64(carID)
	}
	if carRegNum != "" {
		filter.RegNum = &carRegNum
	}
	if carMark != "" {
		filter.Mark = &carMark
	}
	if carModel != "" {
		filter.Model = &carModel
	}
	if carYearString != "" {
		carYear, err := strconv.Atoi(carYearString)
		if err != nil {
			http.Error(w, "Failed to get query parameter year: "+err.Error(), http.StatusBadRequest)
			h.log.Warn("failed to get year", sl.Err(err))
			return
		}
		year := int64(carYear)
		filter.Year = &year
	}
	if carOwnerString != "" {
		carOwner, err := strconv.Atoi(carOwnerString)
		if err != nil {
			http.Error(w, "Failed to get query parameter owner: "+err.Error(), http.StatusBadRequest)
			h.log.Warn("failed to get owner", sl.Err(err))
			return
		}
		owner := int64(carOwner)
		filter.Owner = &owner
	}

	cars, err := h.carsService.GetCars(r.Context(), filter, page, limit)
	if err != nil {
		http.Error(w, "Failed to get cars: "+err.Error(), http.StatusInternalServerError)
	}

	response, err := json.Marshal(map[string][]models.Car{
		"result": cars,
	})
	if err != nil {
		http.Error(w, "Failed to marshal response json: "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "application/json")
	w.Write(response)
}

// @Summary Обновление информации о машине
// @Description Обновление информации о машине по ее идентификатору
// @Tags cars
// @Accept json
// @Produce json
// @Param id path int true "Идентификатор машины"
// @Success 200 {string} string "Машина успешно обновлена"
// @Failure 400 {string} string "Ошибка при обновлении машины"
// @Router /cars/{id} [patch]
func (h *Handler) updateCar(w http.ResponseWriter, r *http.Request) {
	carIDString := mux.Vars(r)["id"]

	carID, err := strconv.Atoi(carIDString)
	if err != nil {
		http.Error(w, "Failed to get query parameter id: "+err.Error(), http.StatusBadRequest)
		h.log.Warn("failed to get id", sl.Err(err))
		return
	}

	var carInfo models.CarUpdate

	d := json.NewDecoder(r.Body)

	err = d.Decode(&carInfo)
	if err != nil {
		http.Error(w, "Failed to parse JSON", http.StatusBadRequest)
		h.log.Warn("failed to parse json", sl.Err(err))
		return
	}
	carInfo.ID = int64(carID)

	err = h.carsService.UpdateCar(r.Context(), carInfo)
	if err != nil {
		if errors.Is(err, cars.ErrInvalidCarID) || errors.Is(err, cars.ErrInvalidPeopleID) {
			http.Error(w, "Failed to update car: "+err.Error(), http.StatusBadRequest)
		} else {
			http.Error(w, "Failed to update car: "+err.Error(), http.StatusInternalServerError)
		}
		return
	}

	w.WriteHeader(http.StatusOK)
}

// @Summary Удаление машины
// @Description Удаление машины по ее идентификатору
// @Tags cars
// @Accept json
// @Produce json
// @Param id path int true "Идентификатор машины"
// @Success 200 {string} string "Машина успешно удалена"
// @Failure 400 {string} string "Ошибка при удалении машины"
// @Router /cars/{id} [delete]
func (h *Handler) deleteCar(w http.ResponseWriter, r *http.Request) {
	carIDString := mux.Vars(r)["id"]

	carID, err := strconv.Atoi(carIDString)
	if err != nil {
		http.Error(w, "Failed to get query parameter id: "+err.Error(), http.StatusBadRequest)
		h.log.Warn("failed to get id", sl.Err(err))
		return
	}

	err = h.carsService.DeleteCarByID(r.Context(), int64(carID))
	if err != nil {
		if errors.Is(err, cars.ErrInvalidCarID) {
			http.Error(w, "Failed to delete car: "+err.Error(), http.StatusBadRequest)
		} else {
			http.Error(w, "Failed to delete car: "+err.Error(), http.StatusInternalServerError)
		}
		return
	}

	w.WriteHeader(http.StatusOK)
}
