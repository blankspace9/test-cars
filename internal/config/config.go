package config

import (
	"time"

	"github.com/ilyakaznacheev/cleanenv"
	"github.com/joho/godotenv"
)

type (
	Config struct {
		Env         string `env:"ENV" env-default:"local"`
		ExternalAPI string `env:"EXTERNAL_API_URL" env-required:"true"`
		HTTPServer  Server
		Storage     Postgres
	}

	MigrateConfig struct {
		Storage Postgres
	}

	Server struct {
		Port    string        `env:"HTTP_PORT" env-default:"8080"`
		Timeout time.Duration `env:"TIMEOUT"`
	}

	Postgres struct {
		Host     string `env:"HOST" env-default:"localhost"`
		Port     string `env:"PORT" env-default:"5432"`
		Username string `env:"USER" env-default:"postgres"`
		DBName   string `env:"NAME"`
		SSLMode  string `env:"SSLMODE" env-default:"disable"`
		Password string `env:"PASSWORD"`
	}
)

func MustLoad() *Config {
	var cfg Config

	if err := godotenv.Load(".env"); err != nil {
		panic("failed to load .env file: " + err.Error())
	}

	if err := cleanenv.ReadEnv(&cfg); err != nil {
		panic("failed to read config: " + err.Error())
	}

	return &cfg
}

func MigrateMustLoad() *MigrateConfig {
	cfg := new(MigrateConfig)

	if err := godotenv.Load(".env"); err != nil {
		panic("failed to load .env file: " + err.Error())
	}

	if err := cleanenv.ReadEnv(&cfg.Storage); err != nil {
		panic("failed to read config: " + err.Error())
	}

	return cfg
}
