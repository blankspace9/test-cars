package app

import (
	"log/slog"

	httpapp "github.com/blankspace9/task-cars/internal/app/http"
	"github.com/blankspace9/task-cars/internal/config"
	"github.com/blankspace9/task-cars/internal/delivery/rest"
	"github.com/blankspace9/task-cars/internal/services/cars"
	"github.com/blankspace9/task-cars/internal/storage/postgres"
)

type App struct {
	HTTPServer *httpapp.App
}

func New(log *slog.Logger, cfg config.Config) *App {
	storage, err := postgres.New(postgres.PostgresConnectionInfo(cfg.Storage))
	if err != nil {
		panic(err)
	}

	carsService := cars.New(log, cfg.ExternalAPI, storage, storage)

	handler := rest.New(log, carsService)

	httpApp := httpapp.New(log, handler.InitRouter(), cfg.HTTPServer.Port, cfg.HTTPServer.Timeout)

	return &App{
		HTTPServer: httpApp,
	}
}
