package cars

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"net/url"

	"github.com/blankspace9/task-cars/internal/domain/models"
	"github.com/blankspace9/task-cars/internal/lib/logger/sl"
	"github.com/blankspace9/task-cars/internal/storage"
)

type Cars struct {
	log            *slog.Logger
	externalAPI    string
	carProvider    CarProvider
	peopleProvider PeopleProvider
}

type CarProvider interface {
	AddCars(ctx context.Context, cars []models.ExternalCar) error
	GetCarByID(ctx context.Context, id int64) (models.Car, error)
	GetCarsPaginate(ctx context.Context, filter models.CarUpdate, page, limit int) ([]models.Car, error)
	UpdateCar(ctx context.Context, new models.CarUpdate) error
	DeleteCarByID(ctx context.Context, id int64) error
}

type PeopleProvider interface {
	AddPeople(ctx context.Context, people models.People) (int64, error)
	DeletePeople(ctx context.Context, id int64) error
}

var (
	ErrInvalidCarID    = errors.New("invalid car ID")
	ErrCarExists       = errors.New("car already exists")
	ErrInvalidPeopleID = errors.New("invalid people ID")
	ErrPeopleExists    = errors.New("owner already exists")
)

func New(log *slog.Logger, externalAPI string, carProvider CarProvider, peopleProvider PeopleProvider) *Cars {
	return &Cars{
		log:            log,
		externalAPI:    externalAPI,
		carProvider:    carProvider,
		peopleProvider: peopleProvider,
	}
}

func (c *Cars) DeleteCarByID(ctx context.Context, id int64) error {
	const op = "cars.DeleteCarByID"

	log := c.log.With(slog.String("op", op))

	log.Info("deleting car by id")

	err := c.carProvider.DeleteCarByID(ctx, id)
	if err != nil {
		if errors.Is(err, storage.ErrCarNotFound) {
			c.log.Warn("car not found", sl.Err(err))

			return fmt.Errorf("%s: %w", op, ErrInvalidCarID)
		}

		c.log.Error("failed to delete car", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	log.Info("car deleted successfully")

	return nil
}

func (c *Cars) UpdateCar(ctx context.Context, new models.CarUpdate) error {
	const op = "cars.UpdateCar"

	log := c.log.With(slog.String("op", op))

	log.Info("updating car")

	err := c.carProvider.UpdateCar(ctx, new)
	if err != nil {
		if errors.Is(err, storage.ErrCarNotFound) {
			c.log.Warn("car not found", sl.Err(err))

			return fmt.Errorf("%s: %w", op, ErrInvalidCarID)
		} else if errors.Is(err, storage.ErrPeopleNotFound) {
			c.log.Warn("owner not found", sl.Err(err))

			return fmt.Errorf("%s: %w", op, ErrInvalidPeopleID)
		}

		c.log.Error("failed to update car", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	log.Info("car updated successfully")

	return nil
}

func (c *Cars) GetCars(ctx context.Context, filter models.CarUpdate, page, limit int) ([]models.Car, error) {
	const op = "cars.GetCars"

	log := c.log.With(slog.String("op", op))

	log.Info("getting cars")

	cars, err := c.carProvider.GetCarsPaginate(ctx, filter, page, limit)
	if err != nil {
		c.log.Error("failed to get cars", sl.Err(err))

		return nil, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("cars got successfully")

	return cars, nil
}

func (c *Cars) AddCars(ctx context.Context, regNums []string) error {
	const op = "cars.AddCars"

	log := c.log.With(slog.String("op", op))

	log.Info("adding cars")

	path, err := url.JoinPath(c.externalAPI, "/info")
	if err != nil {
		c.log.Error("failed to add cars", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	var cars []models.ExternalCar
	for _, num := range regNums {
		resp, err := http.Get(fmt.Sprintf("%s?regNum=%s", path, num))
		if err != nil {
			c.log.Error("failed to add cars", sl.Err(err))

			return fmt.Errorf("%s: %w", op, err)
		}
		defer resp.Body.Close()

		if resp.StatusCode != http.StatusOK {
			c.log.Error("failed to add cars", errors.New(resp.Status))

			return fmt.Errorf("%s: %w", op, errors.New(resp.Status))
		}

		var car models.ExternalCar
		d := json.NewDecoder(resp.Body)

		err = d.Decode(&car)
		if err != nil {
			c.log.Error("failed to add cars", sl.Err(err))

			return fmt.Errorf("%s: %w", op, err)
		}

		cars = append(cars, car)
	}

	err = c.carProvider.AddCars(ctx, cars)
	if err != nil {
		c.log.Error("failed to add cars", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	log.Info("cars added successfully")

	return nil
}
